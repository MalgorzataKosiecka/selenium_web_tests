from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from webdriver_manager.chrome import ChromeDriverManager

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("https://google.com")
driver.maximize_window()

consent_button = driver.find_element_by_xpath("//*[@id='L2AGLb']/div")
consent_button.click()

search_input = driver.find_element_by_xpath("//input")
search_input.send_keys("selenium webdriver")
search_input.send_keys(Keys.ENTER)

driver.close()